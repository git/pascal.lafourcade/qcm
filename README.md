# QCM Latex

## Fonctionnement

Pour que l'executable **execQCMLatex** fonctionne il faut avoir un fichier qui s'appel **test.html** dans son repertoire, qui est generé par la commande `htlatex test1.tex`

De plus il est nécéssaire d'avoir une connection internet car la feuille de style est généré en ligne.

Deux nouveaux fichier seront généré :
*  output.js : Contient tout le code JS qui sera affiché
*  test-js.html : Contient la feuille de style et lance le script de output.js